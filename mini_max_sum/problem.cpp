#include <algorithm>
#include <cstdint>
#include <iostream>
#include <bits/stdc++.h>

using namespace std;

vector<string> split_string(string);

// Complete the miniMaxSum function below.
void calcMiniMaxSum(vector<uint64_t> &arr)
{
    // if the data is in sorted order then we can easily make assumptions about
    // it's min and max values when summed up
    // so we sort the data
    sort(arr.begin(), arr.end());
    // the condition of wanting a sum of 4 elements and that the data is always 5 elements
    // is already given to us so all we need to do is sum up the 1st for elements 
    // for min and sum the last 4 elements for max
    const uint64_t min = arr[0] + arr[1] + arr[2] + arr[3];
    const uint64_t max = arr[1] + arr[2] + arr[3] + arr[4];

    cout << min << " " << max << endl;
}

void printMiniMaxSum(vector<uint64_t> &arr) {
    cout << "Contents of input vector: ";
    for (auto it: arr)
    {
        cout << it << " ";
    }
}

int main(void)
{
    string arr_temp_temp;
    getline(cin, arr_temp_temp);

    vector<string> arr_temp = split_string(arr_temp_temp);

    vector<uint64_t> arr(5);

    for (uint64_t i = 0; i < 5; i++) {
        uint64_t arr_item = stoi(arr_temp[i]);

        arr[i] = arr_item;
    }

    calcMiniMaxSum(arr);

    return 0;
}

vector<string> split_string(string input_string) {
    string::iterator new_end = unique(input_string.begin(), input_string.end(), [] (const char &x, const char &y) {
        return x == y and x == ' ';
    });

    input_string.erase(new_end, input_string.end());

    while (input_string[input_string.length() - 1] == ' ') {
        input_string.pop_back();
    }

    vector<string> splits;
    char delimiter = ' ';

    size_t i = 0;
    size_t pos = input_string.find(delimiter);

    while (pos != string::npos) {
        splits.push_back(input_string.substr(i, pos - i));

        i = pos + 1;
        pos = input_string.find(delimiter, i);
    }

    splits.push_back(input_string.substr(i, min(pos, input_string.length()) - i + 1));

    return splits;
}
