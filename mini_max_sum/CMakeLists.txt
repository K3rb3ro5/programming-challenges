cmake_minimum_required (VERSION 3.8)

set (CMAKE_C_COMPILER "/usr/bin/clang")
set (CMAKE_CXX_COMPILER "/usr/bin/clang++")

project (mini_max_sum
         LANGUAGES C CXX)
include (CheckIncludeFiles)

set (CMAKE_BUILD_TYPE Debug)

set (options
     -Wall
     -Wextra
     -pedantic)


#CPP files to compile
set (proj_src
     problem.cpp
    )

set (target ${PROJECT_NAME})

add_executable (${PROJECT_NAME} ${proj_src})
target_compile_features (${target} PRIVATE cxx_std_17)
target_compile_options (${target} PRIVATE ${options})
